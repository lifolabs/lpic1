# Linux Professional Institute Certification 102

## Welcome

### Please Choose Your Chanpter

-    [Detailed Description Of All Chapters](./Detailed.md)
-    [Shells and Shell Scripting](105_Shell_and_Shell_Scripting/README.md)
-    [Interfaces and Desktops](106_User_Interface_and_Desktops/README.md)
-    [Administrative Tasks](107_Administrative_tasks/README.md)
-    [Essential System Services](108_Essential_System_Services/README.md)
-    [Networking Fundamentals](109_Networking_Fundamentals/README.md)
-    [Security](110_Security/README.md)

