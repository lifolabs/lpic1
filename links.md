# Links to variaous projects

- [VaioLabs Slides](https://slides.vaiolabs.com)
- [LifoLabs Slides](https://slides.lifolabs.com)
- [Code Maven slides](https://code-maven.com/slides/)
- [Howtoforge](https://www.howtoforge.com/)
- [Server world](https://www.server-world.info/en/)
- [Hackr](https://hackr.io/)
- [Github](https://github.com/silent-mobius)
- [Gitlab](https://gitlab.com/silent-mobius)
- [All IT Books](https://www.allitebooks.org/)
