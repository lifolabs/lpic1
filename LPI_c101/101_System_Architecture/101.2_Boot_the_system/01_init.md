# Init

## Some History

- created by Miquelabs Smoorenberg
- init: nickname of the monster in RAM named initialization.
- based on the Unix Sys-V project.

-  in short: services are start one by one, from the moment Kernel runs.

---
## The init process is faily short


init  --> /sbin/init --> /etc/inittab


---
## Runlevels

Runlevel | Purpose  |  Explain
-------- | -------- | -------
0  |  Halt  | Stopping the system, yet the power still runs.
1  |  `Single user` Mode   |
2  |  `Multi-user` mode no networking |
3  |  `Multi-user` mode with `networking` |
4  |  Custom  |
5  |  `Multi-user`, with `networking` and a `graphical` desktop | 
6|  Reboot | Restarting the System



---

## initab configuration

the config file is located at `/etc/inittab`

```conf

```