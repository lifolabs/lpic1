# Linux Boot Sequence

## What happens when Linux OS boots up

- power on
- POST
- Basic Input Output System
- Boot Sector Zero
- The Linux Kernel
    - Initialization ( of the System)
    - Init (of Kernel in HW RAM)

## Tools:

- dmesg
- journalctl -k