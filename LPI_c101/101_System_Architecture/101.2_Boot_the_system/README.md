# Boot The System

##### `Note`: Please Read According To Order

- [Boot Process](./00_boot_process.md)
- [Initialization](./01_init.md)
- [Upstart](./02_upstart.md)
- [Systemd](./03_systemd.md)
- [Legacy Grub](./04_legacy_grub.md)
- [Grub2](./05_grub2.md)
