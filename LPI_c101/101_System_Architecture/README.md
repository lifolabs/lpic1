# System Architecture

- [101.1: Determine and config the hardware settings](./LPI_c101/101_System_Architecture/101.1_Determine_and_configure_hardware_settings/README.md)
- [101.2: Boot The System](./101.2_Boot_the_system/README.md)
